# SPML-5Axis

This project is a child of [SPML CNC machine](https://gitlab.com/fablabbcn-projects/cnc-machines/six-pack-cnc/-/tree/master/) which adding 2 extra axes to it.

**For more updated documentation on this machine please visit the following [link](http://fabacademy.org/2020/labs/barcelona/students/arman-najari/assignments/week17/).**

## Motivation

The main motivation of developing this machine is to maximize features of SPML for producing complex molds.
Also we tend to keep the size small but maybe exceeding couple of centimeter in order to avoid collision for rotational axis.



## Electronic

The main board of this machine will be based on [Barduino](https://gitlab.fabcloud.org/barcelonaworkshops/barduino-2.0) we might change the layout but for now we will use the version 2.2 of this board.

For the shield we will use the same schematic as SPML shield however we need to add extra [pololue](https://www.pololu.com/product/1182/specs) stepper driver.

- [Barduino shield 5axis](https://gitlab.com/armannd092/barduino-cnc-shield)
---
## Firmware

The Firmware that we choose for this machine is [GRBL](https://github.com/grbl/grbl/wiki) which is a open source. We want to combine a branch of this Firmware which adapted for esp32 dev-kit and another fork which has been developed for 5axis CNC but on Arduino mega board.

- [grbl esp32](https://github.com/bdring/Grbl_Esp32/tree/master)
- [grbl 5axis](https://github.com/fra589/grbl-Mega-5X)


---

## Hardware

For the Hardware we followed the same component as the initial design.
Moreover for the extra axis we use [WPM35S-048](https://es.aliexpress.com/item/4000892066188.html?spm=a2g0o.detail.1000013.5.67906fa3Oe0ydA&gps-id=pcDetailBottomMoreThisSeller&scm=1007.13339.146401.0&scm_id=1007.13339.146401.0&scm-url=1007.13339.146401.0&pvid=780ebf58-ece7-4d64-9380-6c0d8a0dee6e&_t=gps-id:pcDetailBottomMoreThisSeller,scm-url:1007.13339.146401.0,pvid:780ebf58-ece7-4d64-9380-6c0d8a0dee6e,tpp_buckets:668%230%23131923%2319_668%23808%234093%2342_668%23888%233325%239_668%232846%238113%23647_668%232717%237567%23956).
