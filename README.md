# SPML a.k.a SixPackMobileLab or SimplePersonalMiniLab

![](img/PCBCNCfinal.png)

The story
--
This pico machine started as a project to make a fab academy kit machine to help the students develope their FabAcademy assignments during the COVID-19 confinement.
This machine was inspired by the pico dvdroom recycled motor based machine DIY projects but translated to a fablab environment was designed by Eduardo Chamorro Martin, Oscar Gonzalez and Josep Marti in Fab Lab Barcelona 2020.
It is a 1.0 version with 3 diferent models (3mm closed case, 5mm closed case,5mm open case)

![](img/PCBCNCdual.jpg)
*Closed Case on the left - OpenCase on the right*

![](img/e1.jpg)
![](img/e2.jpg)


There is two options for fabrication: lasercutted/3dprinted.

![](img/3dversion.png)

What is SPML?
--

SPML is a  fabbable and open source mini machine,which bode can lasercutted or 3dprinted and with fully Fabable electronics boards. This is a 3 axis machine with a tool work area of 100x100x80mm and exterior footprint of 150x150x200 (almost the same a pack of six cans) that make it really portable,lightweight and extremely low cost.

![](img/PCBCNC-sixpackcomparison.png)

Main **features** of this version are:

- Low material usage,body can be fabricated with a 600x400x3 board
- Uses low current motors so could be driven with 5v
- 100% compatible with **default Arduino IDE**
- Fabbable electronics (fablab inventory components)
- ESP32 control board with usb-wifi-bluetooth control
- Modular tool head (laser and cnc)
- Standart GCODE control
- Accurate upto 0.1mm
- Self Publish Web Control interface,no need to install a control software
- Compact(can be carried in a backpack) and lightweight <2kg
- Tiny linear motion system that reduces the total cost to <60 euros.
- Open case version(better for modifications) or closed(better for travelling)
- Modular electronics.Base [BARDUINO BOARD](https://gitlab.fabcloud.org/barcelonaworkshops/barduino-2.0) + [CUSTOM CNC SHIELD](https://gitlab.com/fablabbcn-projects/electronics/cnc-shield)
- Integrated one hole in the base to clean up the dust acumulated in the base
- High mobility, **ability to be run on batteries.**

This has been the evolution of the machine until now:

![](img/PCBCNC-evolution.jpg)


**Update on ELECTRONIC CONTROLBOARD - [Barduino+CNCShield+Casing](https://gitlab.com/fablabbcn-projects/electronics/barduino-cnc-shield)
![](img/IMG_0176.jpg)

### 3D printed version

-all the parts fit in a standart 200x200x200 fdm 3d printer
-almost no support
-max time of file 12hours at 0.3 layer height
- less than 1kg( one spool) of material
- less hardware assembly required compared to lasercutter option
- all the electronic is embedded in the frame and motors and cables are protected

Blue parts are the 3d printed parts.
![](img/3d1.png)
![](img/3d2.png)

Front view

![](img/3d3.png)

Back part - electronic enclosure

![](img/3d4.png)


Size Commparison to other FabLabNetwork fabable machines:

[BigFDM](https://github.com/fab-machines/BigFDM) VS [SPML]()

![](img/sizecomparison.jpg)

### B.O.M

![](img/PCBCNCopen.jpg)

#### Hardware

- 3x [motor+railsystems 100mm microsteppers](https://es.aliexpress.com/item/32838685026.html) we choose this as the power train due to its small size,powerconsumption,travel and cost.

![](components-reference/HTB1eJSJdOMnBKNjSZFCq6x0KFXax.jpg)

- 1x [dc motor rs555](https://es.aliexpress.com/item/4000341335374.html?spm=a2g0o.productlist.0.0.38c815e9rnbPGx&algo_pvid=f73d2d42-dc19-4fb9-8600-52978af4372b&algo_expid=f73d2d42-dc19-4fb9-8600-52978af4372b-7&btsid=0ab50f6215868560219372128e9bce&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)Standart 555 size dc motor up to 17.000rpm at 12v

![](components-reference/rs555-high-speed-dc-motor-500x500.jpg)

- 1x [simple collet 3.175mm to 3.175mm](https://es.aliexpress.com/item/33006225788.html?spm=a2g0o.productlist.0.0.122f624fT5Sw7h&algo_pvid=f69dca86-d6e2-41b4-946c-9044ad2d52b1&algo_expid=f69dca86-d6e2-41b4-946c-9044ad2d52b1-16&btsid=0ab6f81615875556011494701e3c63&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)

![](components-reference/HTB10KVLd21H3KVjSZFBq6zSMXXaR.jpg)

- 3x [microendstop](https://es.aliexpress.com/item/32811306212.html?spm=a2g0o.productlist.0.0.3ca45015HlylXx&algo_pvid=568111d3-5f54-44c2-a933-fcb018bb94b2&algo_expid=568111d3-5f54-44c2-a933-fcb018bb94b2-0&btsid=0ab6fab215875559809805163e7509&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)Any normal 3d printer endstop will work we choose a small one to make it easier to fit.

- 3x [stepper drivers a4988](https://es.aliexpress.com/item/32963690420.html?spm=a2g0o.productlist.0.0.188f13cfg8fht3&algo_pvid=4728b159-1783-4dec-ab73-b7ae2c59251f&algo_expid=4728b159-1783-4dec-ab73-b7ae2c59251f-5&btsid=0ab50f4915875553362458263e4099&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)

- [screw set 3mm](https://es.aliexpress.com/item/33022099174.html?spm=a2g0o.productlist.0.0.d226634ccVPJn3&algo_pvid=5ab92efb-5c8b-4233-8e00-ea0b69b9195b&algo_expid=5ab92efb-5c8b-4233-8e00-ea0b69b9195b-54&btsid=0be3743615867690564323674eb2ae&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)We choose this kit as it contains all the screws needed(in particular we need :30 x m3-20mm screws + 30 x m3nuts)

- 1x [powersupply 12v 6a eu plug](https://es.aliexpress.com/item/4000101239593.html?spm=a2g0o.productlist.0.0.1de8136595uiRu&s=p&ad_pvid=202004130353107066882118840320010040141_2&algo_pvid=d05e42f2-e110-4a81-8c6d-c000c35b810c&algo_expid=d05e42f2-e110-4a81-8c6d-c000c35b810c-1&btsid=0ab6d69515867751907388027e76b3&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)

- [screws flathead m2](https://es.aliexpress.com/item/32981714992.html?spm=a2g0o.productlist.0.0.7ca0120aYbrEDT&algo_pvid=07937ddc-d50f-4705-83a8-4b8cdf98817b&algo_expid=07937ddc-d50f-4705-83a8-4b8cdf98817b-2&btsid=0ab50f4415875569076956329eda5b&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)We choose this kit as it contains all the screws needed(in particular we need :20 x m2-8mm screws)

![](components-reference/HTB10HSwcTZmx1VjSZFGq6yx2XXaI.jpg)

**IMPORTANT, THESE SCREWS ARE FLATHEAD!!**

- [nuts screw m2](https://es.aliexpress.com/item/32988995881.html?spm=a2g0o.productlist.0.0.983d5c7dHz2NDS&algo_pvid=e98fb379-649d-4005-8561-09fbf877e1cf&algo_expid=e98fb379-649d-4005-8561-09fbf877e1cf-1&btsid=0ab6fab215875569817915412e741c&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_)We choose this kit as it contains all the screws needed(in particular we need :20 x m2-nuts)

- [600x400x3/5mm material(our choice was acrylic)](https://www.mwmaterialsworld.com/es/materiales/metacrilato/plancha-de-metacrilato-fluorescente-colores.html)This material opcion is the most expensive one to look for an alternative you can use mdf boards

#### Electronics

*All the electronic components are standard fabacademy inventory*

 - 1x [WIFI MODULE 32MBITS SPI FLASH](https://www.digikey.es/products/en?keywords=1904-1023-1-ND)
 - 1x [RES 10.0K OHM 1/4W 1% 1206 SMD](https://www.digikey.com/products/en?keywords=311-10.0KFRCT-ND)
 - 1x [SWITCH TACT SMD W-GND 160GF](https://www.digikey.com/product-detail/en/B3SN-3112P/SW262CT-ND)
 - 2x [CAP CER 10UF 35V Y5V 1206](https://www.digikey.com/product-detail/en/B3SN-3112P/SW262CT-ND)
 - 1x [ IC REG LDO 3.3V 1A SOT223-3](https://www.digikey.es/products/en?keywords=ZLDO1117G33DICT-ND)
 - 2x [LED RED CLEAR 1206 SMD](www.digikey.com/products/en?keywords=160-1167-1-ND)
 - 2x [RES 100 OHM 1/4W 1% 1206 SMD](https://www.digikey.com/products/en?keywords=311-100FRCT-ND)
 - 1x [SWITCH SLIDE SPDT 12V 100MA GW-](https://www.digikey.es/products/en?keywords=401-2012-1-ND)
 - 1x [ CAP CERAMIC .1UF 250V X7R 1206](www.digikey.com/products/en?keywords=399-4674-1-ND%09)

 ![](img/Barduino2.0.png)

### What you will find in this repo.

- Kicad Design Files + fab library + esp32 library + Design Rules
- GRBL firmware + WebUi
- Png files for producing the boards
- Diagrams of connections
- Libraries install for arduino
- Lasercutter ready files for 3 and 5mm material
- 3d stl files in case you want to 3d print the casing(you need a least a 220x220 3d printer)

**Downloads** *Right click - save as*

*ControlBoards*

Currently, *the control boards are under development*. It's based on an Barduino (ESP32) with micro usb.

[Barduino](https://gitlab.fabcloud.org/barcelonaworkshops/barduino-2.0):

- [Kicad Barduino sch](https://gitlab.fabcloud.org/barcelonaworkshops/barduino-2.0/-/blob/master/BarDuino2.2.micro-KicadFiles/BarDuino2.2.micro.sch)
- [Kicad Barduino brd](https://gitlab.fabcloud.org/barcelonaworkshops/barduino-2.0/-/blob/master/BarDuino2.2.micro-KicadFiles/BarDuino2.2.micro.kicad_pcb)
- [Kicad Barduinolibraries ESP32]()

[Barduino CNC Shield](https://gitlab.com/fablabbcn-projects/electronics/barduino-cnc-shield):

- [Kicad Barduino Shield Sch]()
- [Kicad Barduino Shield brd]()

CNC:

- [Laser Cutter File 5mm version](LASER-cuttingfiles500x600x5mm.dxf)
- [3dFile- Backplate](stl/backplate.dxf)
- [3dFile- Base](stl/base.stl)
- [3dFile- cuttingplate](stl/cuttingplate.stl)
- [3dFile- frontplate](stl/frontplate.stl)
- [3dFile- motorholder](stl/motorholder.stl)
- [3dFile- sideplate x2](stl/sideplate.stl)
- [3dFile- top](stl/top.stl)

### Arduino ESP32 LibrariesRequired

```
http://arduino.esp8266.com/stable/package_esp8266com_index.json
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
https://dl.espressif.com/dl/package_esp32_index.json
```

### Using GRBLESP32

SelfPublish web interface to control the machine through the brower via wifi or bluetooth throught a cncgbrl app.

![](https://raw.githubusercontent.com/luc-github/ESP3D-WEBUI/master/images/Full1.PNG)

[COMPLETE INFORMATION OF THE GRBLESP32 ORIGINAL WIKI](https://github.com/bdring/Grbl_Esp32/wiki/Development-Roadmap)

[COMPLETE INFORMATION OF THE WEB UI CONTROL - ORIGINAL REPO](https://github.com/luc-github/ESP3D-WEBUI)

The ESP32 is potentially a great target for Grbl for the following reasons

- Faster - At least 4x the step rates over Grbl
- Lower Cost
- Small footprint
- More Flash and RAM - A larger planner buffer could be used and more features could be added.
- I/O - It has just about the same number of pins as an Arduino UNO, the original target for Grbl
- Peripherals - It has more timers and advanced features than an UNO. These can also be mapped to pins more flexibly.
- Connectivity - Bluetooth and WiFi built in.
- Fast Boot - Boots almost instantly and does not need to be formally shutdown (unlike Raspberry Pi or Beagle Bone)
- RTOS (Real Time operating System) - Custom features can be added without affecting the performance of the motion control system.

The code should be compiled using the latest Arduino IDE. [Follow instructions](https://github.com/espressif/arduino-esp32) here on how to setup ESP32 in the IDE. The choice was made to use the Arduino IDE over the ESP-IDF to make the code a little more accessible to novices trying to compile the code.

I use the ESP32 Dev Module version of the ESP32. I suggest starting with that if you don't have hardware yet.

For basic instructions on using Grbl use the [gnea/grbl wiki](https://github.com/gnea/grbl/wiki). That is the Arduino version of Grbl, so keep that in mind regarding hardware setup.

### Extras

We recommend you to use a Gcode visualizer before sending the Files to the machine.

We have recompiled this minimalistic version [LINK TO Gcode WebVisualizer](https://fablabbcn-projects.gitlab.io/cnc-machines/g-code-visualizer/)

[MORE PHOTOS ON DRIVE](https://photos.google.com/share/AF1QipOf1FCZwninH2g2TozmkrkOHxgymzGBk3E0FDfvesJRGzmMalfACGrS2di01M0-Pw?key=R0Y4NjFiZzBIazBvTkcyUUtSRnhsOGFqUi1ESFlR)

### Contact

- **eduardo.chamorro@iaac.net**
- **oscar@fablabbcn.org**
- **josep@fablabbcn.org**

Thanks

### License

GNU General Public License v3.0 for software (original software credited below). (See LICENSE file).

Cern OHL V1.2 for the Machine Files. (See CERN_OHL file)

### Credits

The original [Grbl](https://github.com/gnea/grbl) is an awesome project by Sungeon (Sonny) Jeon.

The Wifi and WebUI is based on this [project](https://github.com/luc-github/ESP3D-WEBUI)Originaly based on great UI from Jarek Szczepanski.

##### Disclaimer

<div class="align-justify">
This hardware/software is provided "as is", and you use the hardware/software at your own risk. Under no circumstances shall any author be liable for direct, indirect, special, incidental, or consequential damages resulting from the use, misuse, or inability to use this hardware/software, even if the authors have been advised of the possibility of such damages.</div>
